import { cubicBezier } from './cubic-bezier'

type easeFn = (x:number) => number

export const linear = (x:number) => x
export const in2 = (x:number) => x < 0 ? 0 : x > 1 ? 1 : (x * x)
export const in3 = (x:number) => x < 0 ? 0 : x > 1 ? 1 : (x * x * x)
export const in4 = (x:number) => x < 0 ? 0 : x > 1 ? 1 : (x * x * x * x)
export const in5 = (x:number) => x < 0 ? 0 : x > 1 ? 1 : (x * x * x * x * x)
export const out2 = (x:number) => x < 0 ? 0 : x > 1 ? 1 : (1 - (x = 1 - x) * x)
export const out3 = (x:number) => x < 0 ? 0 : x > 1 ? 1 : (1 - (x = 1 - x) * x * x)
export const out4 = (x:number) => x < 0 ? 0 : x > 1 ? 1 : (1 - (x = 1 - x) * x * x * x)
export const out5 = (x:number) => x < 0 ? 0 : x > 1 ? 1 : (1 - (x = 1 - x) * x * x * x * x)

const easeTableNoArgs:Record<string, easeFn> = {
  linear,
  in2,
  in3,
  in4,
  in5,
  out2,
  out3,
  out4,
  out5,
}



/**
 * https://www.desmos.com/calculator/pci6gv2fwj
 */
export const inout = (p = 3, i = .5) => {

  return (x:number) => {

    return (x === i ? x
      : x < 0 ? 0 : x > 1 ? 1
      : x < i
      ? 1 / Math.pow(i, p - 1) * Math.pow(x, p)
      : 1 - 1 / Math.pow(1 - i, p - 1) * Math.pow(1 - x, p)
    )
  }
}



/**
 * Special ease: begins at 0, rise to 1 then returns to 0
 * from Inigo Quilez "useful functions": 
 * https://www.iquilezles.org/www/articles/functions/functions.htm
 * https://www.desmos.com/calculator/9h6o072i43
 */
export const pcurve = (a = 3, b = 5, k = -1) => {

  k = k !== -1 ? k :
		Math.pow(a + b, a + b) / (Math.pow(a, a) * Math.pow(b, b))

  return (x:number) => {
    return (x <= 0 || x >= 1) ? 0 : k * Math.pow(x, a) * Math.pow(1 - x, b)
  }
}

const easeTableWithArgs:Record<string, (...args:number[]) => easeFn> = {
  inout,
  cubicBezier,
  pcurve,
}

export const resolveEase = (ease:string|easeFn):easeFn => {

  if (typeof ease === 'function') {
    return ease
  }

  const parse = () => {
    const [,a, b = ''] = ease.trim().match(/^([\w-]*)(\(.*\))?$/) ?? []
    const name = a.split('-')
      .map((str, index) => index === 0 ? str.toLowerCase() : str[0].toUpperCase() + str.slice(1).toLowerCase())
      .join('')
    const args = b
      .slice(1, -1)
      .split(/\s*,\s*/)
      .map(x => parseFloat(x))
      .filter(x => !isNaN(x))
    return { name, args }
  }
  
  const { name, args } = parse()

  if (name in easeTableWithArgs) {
    return easeTableWithArgs[name](...args)
  }

  if (name in easeTableNoArgs) {
    return easeTableNoArgs[name]
  }

  return linear
}
