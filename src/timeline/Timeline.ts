import * as flex from './flex'

class Stack<T, U = {}> extends Set<(target:T, ...args:U[]) => void> {
  #target:T
  constructor(target:T) {
    super()
    this.#target = target
  }
  call(...args:U[]) {
    const t = this.#target
    for (const cb of this) {
      cb(t, ...args)
    }
  }
  subscribe(cb:(target:T, ...args:U[]) => void) {
    this.add(cb)
    return () => {
      this.delete(cb)
    }
  }
}

export class TimelineNode extends flex.Node {
  
  style:Record<string, any>
  bounds:flex.Bounds|null = null

  constructor(style:Record<string, any> = {}) {
    super()
    this.style = { size:'1w', ...style }
  }

  // NOTE: this could be optimized by a lazy initializer
  onHeadEnter:Stack<TimelineNode, HeadPosition> = new Stack<TimelineNode, HeadPosition>(this)
  onHeadLeave:Stack<TimelineNode, HeadPosition> = new Stack<TimelineNode, HeadPosition>(this)
  onHeadInside:Stack<TimelineNode, HeadPosition> = new Stack<TimelineNode, HeadPosition>(this)
  onHeadThrough:Stack<TimelineNode, HeadPosition> = new Stack<TimelineNode, HeadPosition>(this)
}

interface HeadPosition {
  head:TimelineHead
  x:number
  t:number
}
class TimelineHead {
  
  #position = -1
  #positionOld = -1
  #timeline:Timeline
  onUpdate = new Stack(this)

  constructor(timeline:Timeline) {
    this.#timeline = timeline
  }
  
  setPosition(value:number) {
    if (this.#position === value) {
      return
    }
    
    this.#positionOld = this.#position
    this.#position = value
    this.onUpdate.call()

    const p = this.#position
    const pOld = this.#positionOld
    for (const node of (this.#timeline.flat({ includeSelf:true }) as Generator<TimelineNode>)) {
      const { bounds } = node
      if (bounds === null) {
        continue
      }
      const { position:min, size } = bounds
      const max = min + size
      const x = (p - min) / size
      const xOld = (pOld - min) / size
      const inside = 0 <= x && x <= 1
      const insideOld = 0 <= xOld && xOld <= 1
      const enter = inside && !insideOld
      const leave = !inside && inside
      const throughTop = p > max && pOld < max
      const throughBottom = p < min && pOld > min
      const headPosition:HeadPosition = {
        head:this,
        x:p - min,
        t:x,
      }
      if (inside) {
        node.onHeadInside.call(headPosition)
      }
      if (enter) {
        node.onHeadEnter.call(headPosition)
      }
      if (leave) {
        node.onHeadLeave.call(headPosition)
      }
      if (throughBottom || throughTop) {
        node.onHeadThrough.call(headPosition)
      }
    }
  }
  
  // shorthands
  get timeline() { return this.#timeline }
  get position() { return this.#position }
  set position(value:number) { this.setPosition(value) }
  get positionOld() { return this.#positionOld }
}

export class Timeline extends TimelineNode {

  heads = [new TimelineHead(this)]

  constructor(style:Record<string, any>|null = null) {
    super({ size:'fit', ...style })
  }

  // shorthands
  get head() { return this.heads[0] }

  toSvgString() {

    const width = 600
    const height = 200
    const margin = 40

    const headLines = this.heads.map(head => {
      const x = margin + head.position / this.bounds!.size * (width - 2 * margin)
      return `<line stroke="red" x1="${x}" x2="${x}" y1="${0}" y2="${height - margin}"></line>`
    })

    return `
<svg width="${width}" height="${height}" xmlns="http://www.w3.org/2000/svg">
${flex.treeToSvgString(this, { width, height, margin })}
${headLines.join('\n')}
</svg>`
  }
}