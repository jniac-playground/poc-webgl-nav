export const className = (...args:any[]) => {
  const isObject = (x:any) => x && typeof x === 'object'
  return (
    args
      .map(value => {
        if (isObject(value)) {
          const entries = Object.entries(value)
          if (entries.length === 1) {
            const [key, value] = entries[0]
            return value && key
          }
        }
  
        return value
      })
      .filter(str => !!str && !isObject(str))
      .join(' ')
  )  
}