interface DragInfo { 
  x:number
  y:number
  dx:number
  dy:number
  down:boolean
  type:'mouse'|'touch'
  startDirection:'horizontal'|'vertical'
}

interface DragHandlerArgs {
  element:HTMLElement,
  onDrag:(delta:DragInfo) => void
  type?:string 
}

/**
 * Drag handler will call 'onDrag' with info about the current drag.
 */
export const getDragHandler = ({
  element = document.body,
  type = 'mouse,touch',
  onDrag,
}:DragHandlerArgs) => {

  const document = element.getRootNode() as HTMLDocument
  const onDestroy = new Set<() => void>()
  const destroy = () => {
    for (const cb of onDestroy) {
      cb()
    }
  }

  if (type.includes('mouse')) {
    
    let count = 0
    const info:DragInfo = { x:0, y:0, dx:0, dy:0, down:false, type:'mouse', startDirection:'horizontal' }
    
    const onMouseDown = (event:MouseEvent) => {
      info.down = true
      info.x = event.x
      info.y = event.y
      count = 0
    }
    
    const onMouseUp = () => info.down = false
    
    const onMouseMove = (event:MouseEvent) => { 
      if (info.down) {
        const { x, y } = event
        info.dx = x - info.x
        info.dy = y - info.y
        info.x = x
        info.y = y
        if (count === 0) {
          info.startDirection = Math.abs(info.dx) > Math.abs(info.dy) ? 'horizontal' : 'vertical'
        }
        onDrag({ ...info })
        count++
      }
    }
    
    element.addEventListener('mousedown', onMouseDown)
    element.addEventListener('mousemove', onMouseMove)
    document.addEventListener('mouseup', onMouseUp, { capture:true })

    onDestroy.add(() => {
      element.removeEventListener('mousedown', onMouseDown)
      element.removeEventListener('mousemove', onMouseMove)
      document.removeEventListener('mouseup', onMouseUp)
    })
  }

  if (type.includes('touch')) {
      
    let count = 0
    const info:DragInfo = { x:0, y:0, dx:0, dy:0, down:false, type:'touch', startDirection:'horizontal' }
    
    const onTouchStart = (event:TouchEvent) => {
      const { clientX:x, clientY:y } = event.touches[0]
      info.down = true
      info.x = x
      info.y = y
      count = 0
    }
  
    const onTouchEnd = () => info.down = false
    
    const onTouchMove = (event:TouchEvent) => { 
      if (info.down) {
        const { clientX:x, clientY:y } = event.touches[0]
        info.dx = x - info.x
        info.dy = y - info.y
        info.x = x
        info.y = y
        if (count === 0) {
          info.startDirection = Math.abs(info.dx) > Math.abs(info.dy) ? 'horizontal' : 'vertical'
        }
        onDrag({ ...info })
        count++
      }
    }
  
    element.addEventListener('touchstart', onTouchStart)
    element.addEventListener('touchmove', onTouchMove)
    document.addEventListener('touchend', onTouchEnd)
    
    onDestroy.add(() => {
      element.removeEventListener('touchstart', onTouchStart)
      element.removeEventListener('touchmove', onTouchMove)
      document.removeEventListener('touchend', onTouchEnd)
    })
  }
  
  return { destroy }
}
