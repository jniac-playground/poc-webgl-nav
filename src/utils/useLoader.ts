import { Loader, Mesh, Object3D } from 'three'

interface LoaderCtor {
  new():Loader
}

interface Getter {
  isDone: () => boolean
  get: () => Object3D|null
  promise: Promise<Object3D>
}

interface Args {
  loader:LoaderCtor
  url:string
  scaleFactor?:number
}

const loaders = new Map<LoaderCtor, () => Getter>()

const loadObject = (args:Args) => {

  const { loader, url } = args

  const createGetter = () => {

    const instance = new loader()
    const cache = new Map<string, Getter>()

    const getter = () => {
      
      const load = () => {
        
        let object:Object3D|null = null
        const get = () => object
        const isDone = () => object !== null
        
        const promise = instance.loadAsync(url)
        
        promise.then(value => {
          object = value
          object!.traverse(child => {
            if (child instanceof Mesh) {
              const { scaleFactor:s = 1 } = args
              child.geometry.scale(s, s, s)
            }
          })
          return object!
        })
        
        promise.catch(e => console.log(e))

        const getter = { get, isDone, promise }
        cache.set(url, getter)
        return getter
      }

      return cache.get(url) ?? load()
    }
    
    loaders.set(loader, getter)

    return getter
  }

  return loaders.get(loader) ?? createGetter()
}

export const useLoader = (args:Args) => {
  const getter = loadObject(args)()
  if (getter.isDone() === false) {
    throw getter.promise
  }
  return getter.get()!
}