export const clamp = (x:number, min = 0, max = 1) => x < min ? min : x > max ? max : x
export const lerp = (a:number, b:number, t:number, clamped = true) => {
  return a + (b - a) * (clamped ? clamp(t) : t)
}