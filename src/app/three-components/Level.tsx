import React, { Suspense, useEffect, useRef } from 'react'
import { GroupProps, MeshProps } from 'react-three-fiber'
import * as THREE from 'three'
import { tween } from '../../anim/Animator'
import { useStore } from '../../state/main'
import { About } from './About'
import { BigLetter } from './BigLetter'
import { WireBox } from './WireBox'

const Box = ({ size = 1, color, sectionIndex, ...props }:{ size?:number, color:any, sectionIndex:number } & MeshProps) => {
  return (
    <group {...props}>
      <About
        sectionIndex={sectionIndex}
        position={[1, 0, size / 2 + .1]}/>
      <mesh castShadow receiveShadow>
        <boxBufferGeometry args={[size, size, size]} />
        <meshStandardMaterial color={color}/>
      </mesh>
    </group>
  )
}

const defaultProps = {
  size: 4,
  color: 'orange',
  sectionIndex: -1,
  reversed: false,
}

export const Level:React.FC<GroupProps & Partial<typeof defaultProps>> = (props) => {
  
  const {
    size,
    color,
    sectionIndex,
    reversed,
    ...rest
  } = { ...defaultProps, ...props }

  let box = new THREE.Vector3(-2.1, 0, 0)
  let wire = new THREE.Vector3(2.1, 0, 0)
  if (reversed) {
    [box, wire] = [wire, box]
  }

  const group = useRef<THREE.Group>()
  const scale = useRef({ value:1 })
  const { sectionIndex:currentSectionIndex } = useStore()
  useEffect(() => {
    const to = sectionIndex === currentSectionIndex ? 1.1 : 1
    const onUpdate = () => group.current?.scale.setScalar(scale.current.value)
    const { cancel } = tween(scale.current, 'value', { to, ease:'out5', duration:.8, onUpdate:onUpdate })
    return cancel
  }, [currentSectionIndex, sectionIndex])

  return (
    <Suspense fallback={null}>
      <group {...rest} ref={group}>
        <group position={wire}>
          <WireBox color={color}/>
          <BigLetter color={color} value={String(sectionIndex + 1)}/>
        </group>
        <Box
          sectionIndex={sectionIndex}
          position={box}
          color={color}
          size={size}/>
      </group>
    </Suspense>
  )
}
