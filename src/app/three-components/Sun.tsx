import React, { useEffect, useRef } from 'react'
import { DirectionalLightProps } from 'react-three-fiber'
import { DirectionalLight } from 'three'

export const Sun:React.FC<DirectionalLightProps> = (props) => {
  const ref = useRef<DirectionalLight>()
  useEffect(() => {
    const light = ref.current!
    const side = 20
    light.shadow.camera.left = -side
    light.shadow.camera.right = side
    light.shadow.camera.top = side
    light.shadow.camera.bottom = -side
  })
  const shadowIntensity = 0.3
  return (
    <>
      <directionalLight
        {...props}
        ref={ref}
        intensity={shadowIntensity}
        castShadow
        shadow-mapSize-height={1024 * 2}
        shadow-mapSize-width={1024 * 2} />
      <directionalLight
        {...props}
        intensity={1 - shadowIntensity} />
    </>
  )
}
