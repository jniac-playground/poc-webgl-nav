import React, { useMemo } from 'react'
import { Mesh, MeshStandardMaterial } from 'three'
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader'
import { useLoader } from '../../utils/useLoader'

export const WireBox = ({ color = 'red', ...props }: any) => {

  const wireBox = useLoader({ loader: FBXLoader, url: 'assets/WireBox.fbx', scaleFactor: 1.98 / 100 })
  const clone = useMemo(() => {
    const clone = wireBox.clone(true)
    clone.traverse(child => {
      if (child instanceof Mesh) {
        child.receiveShadow = true
        child.castShadow = true
        child.material = new MeshStandardMaterial({ emissiveIntensity: .5 })
      }
    })
    return clone
  }, [wireBox])

  useMemo(() => {
    clone.traverse(child => {
      if (child instanceof Mesh) {
        const material = child.material as any
        material.color?.set(color)
        material.emmissive?.set(color)
      }
    })
  }, [clone, color])

  return (
    <primitive object={clone} {...props} />
  )
}
