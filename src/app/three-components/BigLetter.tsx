import React, { FC, useMemo, useRef } from 'react'
import { GroupProps, useFrame, useUpdate } from 'react-three-fiber'
import * as THREE from 'three'
import { Mesh } from 'three'
import Roboto from '../../fonts/Roboto-Slab-Black-Regular.json'

const defaultProps = {
  value: 'X',
  size: 3,
  color: 'orange',
  vAlign: 'center',
  hAlign: 'center',
}

export const BigLetter: FC<GroupProps & Partial<typeof defaultProps>> = (props) => {
  
  const {
    size,
    value,
    color,
    vAlign,
    hAlign,
    ...rest
  } = { ...defaultProps, ...props }

  const config = useMemo(() => {
    const font = new THREE.Font(Roboto)
    return { font, size, height: 1, curveSegments: 32, bevelEnabled: true, bevelThickness: .1, bevelSize: .15, bevelOffset: 0, bevelSegments: 8 }
  }, [size])

  const group = useRef<THREE.Group>()
  
  const mesh = useUpdate(
    (self: Mesh) => {
      const size = new THREE.Vector3()
      self.geometry.computeBoundingBox()
      self.geometry.boundingBox!.getSize(size)
      self.position.x = hAlign === 'center' ? -size.x / 2 : hAlign === 'right' ? 0 : -size.x
      self.position.y = vAlign === 'center' ? -size.y / 2 : vAlign === 'top' ? 0 : -size.y
      self.position.z = -size.z / 2
    },
    [value]
  )

  useFrame(() => {
    group.current?.rotateY(.005)
  })

  return (
    <group ref={group} {...rest}>
      <mesh ref={mesh} castShadow receiveShadow>
        <textBufferGeometry args={[value, config as any]} />
        <meshStandardMaterial color={color} />
      </mesh>
    </group>
  )
}
