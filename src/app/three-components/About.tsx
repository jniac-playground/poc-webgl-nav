import * as THREE from 'three'
import React, { useMemo, useState } from 'react'
import { useThree } from 'react-three-fiber'
import { useStore } from '../../state/main'
import { tweenTimelineHead } from '../../state/timeline'
import Roboto from '../../fonts/Roboto-Slab-Black-Regular.json'

export const About = ({ sectionIndex, position }: { sectionIndex: number; position: any} ) => {

  const textConfig = useMemo(() => {
    const font = new THREE.Font(Roboto)
    return { font, size: .15, height: .02, curveSegments: 32 }
  }, [])
  
  const three = useThree()
  const [hover, setHover] = useState(false)
  const mobileUpscale = three.aspect > 1 ? 1 : 2
  const scale = new THREE.Vector3().setScalar(hover ? 1.1 : 1).multiplyScalar(mobileUpscale)
  three.gl.domElement.style.cursor = hover ? 'pointer' : 'default'


  const { setSidePageExtended } = useStore()

  const click = () => {
    tweenTimelineHead(sectionIndex, { duration: .8, ease: 'out5' })
    setSidePageExtended(true)
  }

  return (
    <group position={position} scale={scale}>
      <mesh castShadow receiveShadow
        position-x={-.4}
        position-y={-.05}>
        <textBufferGeometry args={['about >', textConfig as any]} />
        <meshStandardMaterial color='white' />
      </mesh>
      <mesh
        onPointerOver={() => setHover(true)}
        onPointerOut={() => setHover(false)}
        onClick={click}>
        <boxGeometry args={[1, .4, .5]} />
        <meshBasicMaterial wireframe transparent opacity={0} />
      </mesh>
    </group>
  )
}
