import React, { useEffect, useState } from 'react'
import { LEVEL_HEIGHT } from '../state/config'
import { useStore } from '../state/main'
import { timeline, tweenTimelineHead } from '../state/timeline'
import { TimelineNode } from '../timeline/Timeline'
import { SidePage } from './dom-components/SidePage'
import { TimelineDebugButton, TimelineGraph } from './dom-components/TimelineDebug'
import { Level } from './three-components/Level'
import { Keyboard } from './Keyboard'
import { Stage } from './Stage'
import './App.css'

export default function App() {
  
  const [debug, setTimelineDebug] = useState(false)

  const { sectionIndex, setSectionIndex } = useStore()
  
  useEffect(() => {
    // NOTE: very hacky, update timeline with zero-duration, only if the page 
    // was open recently (less than 500ms), quite ugly, but working...
    if (performance.now() < 500) {
      tweenTimelineHead(sectionIndex, { duration:0 })
    }
  })

  useEffect(() => {
    const unsubscribe = timeline.flatArray({ filter:(n:any) => n.style.type === 'section-trigger'})
      .map((node:TimelineNode) => {
        return node.onHeadEnter.subscribe(() => {
          const newSectionIndex = node.style.sectionIndex
          if (newSectionIndex !== sectionIndex) {
            setSectionIndex(newSectionIndex)
          }
        })        
      })
    return () => {
      for (const f of unsubscribe) f()
    }
  })

  return (
    <div className="App">
      <Keyboard/>
      {!debug ? (
        <TimelineDebugButton onClick={() => setTimelineDebug(true)}/>
      ):(
        <TimelineGraph
          onClick={() => setTimelineDebug(false)}
          style={{ position:'absolute', top:'0', left:'0', padding:'16px', zIndex:10 }}/>
      )}
      <Stage>
        <Level
          sectionIndex={0}/>
        <Level
            sectionIndex={1}
            position-y={-LEVEL_HEIGHT * 1}
            reversed
            color="#09c"/>
          <Level
            sectionIndex={2}
            position-y={-LEVEL_HEIGHT * 2}/>
          <Level
            sectionIndex={3}
            position-y={-LEVEL_HEIGHT * 3}
            reversed
            color="#f58"/>
      </Stage>
      <SidePage/>
    </div>
  )
}
