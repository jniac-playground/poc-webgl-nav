import { useEffect, useRef } from 'react'
import { splitPage, useStore } from '../../state/main'
import { SIDE_MAX } from '../../state/SplitPage'
import sectionJson from '../../state/sections.json'
import './SideWrapper.css'
import { className } from '../../utils/className'

const SideSection = ({ index = 0, ...props }) => {

  const { sectionIndex } = useStore()
  const visible = sectionIndex === index
  
  return (
    <div className={className('SideSection absolute-fill', { visible })}>
      <h2>
        Section&nbsp;
        <span style={{ color:props.colors?.css }}>
          #{index + 1}
        </span> 
      </h2>
      <h1>{props.title}</h1>
      <p>{props.text}</p>
      <div 
        className="button" 
        style={{ backgroundColor:props.colors?.css }}
        onClick={() => alert(`it's a demo only...`)}>
        <span className="im im-angle-right"/>
        Know more
      </div>
    </div>
  )
}

export const SideWrapper = () => {
  const div = useRef<HTMLDivElement>(null)
  useEffect(() => {
    return splitPage.subscribe(opening => {
      div.current!.classList.toggle('visible', opening > .8)
    })
  })

  return (
    <div className="SideWrapper" ref={div} style={{ width:`${SIDE_MAX}px` }}>
      {sectionJson.map((props, index) => (
        <SideSection key={index} index={index} {...props}/>
      ))}
    </div>
  )
}