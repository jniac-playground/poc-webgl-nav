import { useEffect, useRef } from 'react'
import { timeline } from '../../state/timeline'
import { Div } from './types'

export const TimelineDebugButton = (props:React.DOMAttributes<HTMLElement> & React.HTMLAttributes<HTMLElement>) => {
  return (
    <div {...props}
      className="DebugButton absolute-top-left button border" 
      style={{ margin:'8px', padding:'8px', fontSize:'10px' }}>
      Timeline Debug
    </div>
  )
}

export const TimelineGraph:Div = (props:React.DOMAttributes<HTMLElement>) => {
  const div = useRef<HTMLDivElement>(null)
  useEffect(() => {
    const cb = () => {
      if (div.current) {
        div.current.innerHTML = timeline.toSvgString()
      }
    }
    cb()
    return timeline.head.onUpdate.subscribe(cb)
  })
  return (
    <div {...props} className="TimelineInfo">
      <h2>Timeline Debug</h2>
      <div ref={div}></div>
    </div>
  )
}
