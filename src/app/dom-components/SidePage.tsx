import React, { useEffect, useRef } from 'react'
import { LEVEL_COUNT } from '../../state/config'
import { splitPage, useStore } from '../../state/main'
import { SIDE_MAX, SIDE_MIN } from '../../state/SplitPage'
import { tweenTimelineHead } from '../../state/timeline'
import { className } from '../../utils/className'
import { SideWrapper } from './SideWrapper'
import sectionJson from '../../state/sections.json'
import './SidePage.css'

const Burger = () => {
  const {
    sidePageExtended: extended,
    setSidePageExtended: setExtended,
    sectionIndex,
  } = useStore()
  const click = (e: React.MouseEvent<HTMLDivElement, MouseEvent>): void => { 
    e.stopPropagation()
    setExtended(!extended)
  }
  const div = useRef<HTMLDivElement>(null)
  useEffect(() => {
    div.current?.style.setProperty('--color', sectionJson[sectionIndex].colors.css)
  })
  return (
    <div ref={div} className={className('Burger', { cross:extended })} onClick={click}>
      {!extended ? (
        <>
          <div className="plain"/>
          <div className="spacing"/>
          <div className="plain"/>
          <div className="spacing"/>
          <div className="plain"/>
        </>
      ):(
        <>
          <div className="crossbar"/>
          <div className="crossbar"/>
        </>
      )}
    </div>
  )
}

const VerticalNav = () => {
  
  const { sectionIndex } = useStore()
  
  const click = (e: React.MouseEvent<HTMLDivElement, MouseEvent>, navAction:'prev'|'next') => {
    e.stopPropagation()
    e.preventDefault()
    tweenTimelineHead(sectionIndex + (navAction === 'next' ? 1 : -1), { duration:.5 })
  }

  const div = useRef<HTMLDivElement>(null)
  useEffect(() => {
    return splitPage.subscribe(opening => {
      div.current!.style.opacity = (opening > .1 ? 0 : 1).toFixed(2)
    })
  })

  return (
    <div className="VerticalNav" ref={div}>
      <div className={className('im im-angle-right', { disabled:sectionIndex === 0 })}
        onClick={e => click(e, 'prev')}/>
      <div className="text row">
        Section&nbsp;
        <span style={{ color:sectionJson[sectionIndex].colors.css }}>
          #{sectionIndex + 1}
        </span>
      </div>
      <div className={className('im im-angle-left', { disabled:sectionIndex === LEVEL_COUNT - 1 })}
        onClick={e => click(e, 'next')}/>
    </div>
  )
}

export const SidePage = () => {
  const {
    sidePageExtended: extended,
    setSidePageExtended: setExtended,
  } = useStore()

  const div = useRef<HTMLDivElement>(null)
  useEffect(() => {
    return splitPage.subscribe(opening => {
      const x = SIDE_MIN + (SIDE_MAX - SIDE_MIN) * opening
      div.current!.style.width = `${x.toFixed(1)}px`
    })
  })

  return (
    <div
      ref={div}
      style={{ width:'40px', right:'0' }}
      className={className("SidePage", { extended })}
      onClick={() => setExtended(true)}>
      <SideWrapper/>
      <VerticalNav/>
      <Burger/>
    </div>
  )
}

