import { useEffect } from 'react'
import { useFrame, useThree } from 'react-three-fiber'

const DEFAULT_TIME_BEFORE_SLEEP = 4

let cooldown = DEFAULT_TIME_BEFORE_SLEEP
export const requireRendering = (amount:number = DEFAULT_TIME_BEFORE_SLEEP) => {
  cooldown = Math.max(cooldown, amount)
}

/**
 * Enables lazy rendering (save battery).
 */
export const FrameHandler = () => {
  const { invalidate } = useThree()
  useFrame(() => {
    if (cooldown > 0) {
      invalidate()
      cooldown += -1 / 60
    }
  })
  useEffect(() => {
    const wakeUp = () => {
      requireRendering()
      invalidate()
    }
    window.addEventListener('wheel', wakeUp, { capture:true })
    window.addEventListener('mousedown', wakeUp, { capture:true })
    window.addEventListener('mouseup', wakeUp, { capture:true })
    window.addEventListener('mousemove', wakeUp, { capture:true })
    window.addEventListener('touchstart', wakeUp, { capture:true })
    window.addEventListener('touchend', wakeUp, { capture:true })
    window.addEventListener('touchmove', wakeUp, { capture:true })
    window.addEventListener('keydown', wakeUp, { capture:true })
    return () => {
      window.removeEventListener('wheel', wakeUp)
      window.removeEventListener('mousedown', wakeUp)
      window.removeEventListener('mouseup', wakeUp)
      window.removeEventListener('mousemove', wakeUp)
      window.removeEventListener('touchstart', wakeUp)
      window.removeEventListener('touchend', wakeUp)
      window.removeEventListener('touchmove', wakeUp)
      window.removeEventListener('keydown', wakeUp)
    }
  }) 
  return null
}
