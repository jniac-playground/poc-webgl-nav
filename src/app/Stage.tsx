import * as THREE from 'three'
import React, { useEffect, useRef } from 'react'
import { Canvas, useThree } from 'react-three-fiber'
import { splitPage, useStore } from '../state/main'
import { SECTION_HEIGHT, setTouchDelta, timeline } from '../state/timeline'
import { Sun } from './three-components/Sun'
import { LEVEL_HEIGHT } from '../state/config'
import { lerp } from '../utils/MathUtils'
import { SIDE_MAX, SIDE_MIN } from '../state/SplitPage'
import { getDragHandler } from '../utils/DragHandler'
import { FrameHandler } from './FrameHandler'

const Sky = () => {
  return (
    <mesh>
      <meshBasicMaterial side={THREE.BackSide}/>
      <boxGeometry args={[100, 100, 100]}/>
    </mesh>
  )
}

const getRigRotationFromLocalStorage = ():[number, number, number, string?] => {
  const defaultValue:[number, number, number, string?] = [.05, .11, 0]
  try {
    const stored = localStorage.getItem('camera-rig')
    if (stored === null) {
      return defaultValue
    }
    return JSON.parse(stored)
  } catch(e) {
    return defaultValue
  }
}

const Dolly = () => {
  
  const group = useRef<THREE.Group>()
  const three = useThree()

  useEffect(() => {
    const camera = three.camera as THREE.PerspectiveCamera
    camera.fov = 30
    camera.updateProjectionMatrix()
    
    const cameraDistance = camera.aspect > 1 ? 20 : 25 / camera.aspect
    const rig = new THREE.Object3D()
    rig.rotation.set(...getRigRotationFromLocalStorage())
    const target = new THREE.Vector3(0, 0, 0)
    const update = () => {
      rig.updateMatrix()
      const p = new THREE.Vector3(0, 0, cameraDistance)
      p.applyMatrix4(rig.matrix)
      camera.position.copy(p)
      camera.lookAt(target)
    }
    const handler = getDragHandler({
      type: 'mouse,touch',
      element: three.gl.domElement,
      onDrag: ({ dx, dy, type, startDirection }) => {
        if (type === 'mouse' || (type === 'touch' && startDirection === 'horizontal')) {
          rig.rotation.y += -dx * 0.001
          rig.rotation.x += -dy * 0.001
          localStorage.setItem('camera-rig', JSON.stringify(rig.rotation.toArray()))
          update()
        }
      }
    })
    const updateFromTimeline = () => {
      const y = -(timeline.head.position - SECTION_HEIGHT / 2) / SECTION_HEIGHT * LEVEL_HEIGHT
      rig.position.y = y
      target.y = y
      update()
    }
    const timelineUnsubscribe = timeline.head.onUpdate.subscribe(updateFromTimeline)
    updateFromTimeline()
    return () => {
      handler.destroy()
      timelineUnsubscribe()
    }
  }, [three.camera, three.gl.domElement])

  useEffect(() => {
    return splitPage.subscribe(opening => {
      const camera = three.camera as THREE.PerspectiveCamera
      const x = lerp(SIDE_MIN, SIDE_MAX, opening) / 2
      const w = window.innerWidth
      const h = window.innerHeight
      camera.setViewOffset(w, h, x, 0, w, h)
    })
  })

  return (
    <group reg={group}/>
  )
}

const TouchHandler = () => {
  const { gl:{ domElement } } = useThree()
  useEffect(() => {
    const handler = getDragHandler({
      element: domElement,
      type: 'touch',
      onDrag: ({ startDirection, dy }) => {
        if (startDirection === 'vertical') {
          setTouchDelta(-dy * 2)
        }
      },
    })
    return handler.destroy
  }, [domElement])
  return null
}

export const Stage:React.FC = ({ children }) => {
  const { setSidePageExtended } = useStore()
  return (
    <Canvas 
      shadowMap
      invalidateFrameloop
      pixelRatio={window.devicePixelRatio}
      onMouseDown={() => setSidePageExtended(false)}>
      <Sky/>
      <Sun position={[3, 5, 2]}/>
      <ambientLight/>
      <FrameHandler/>
      <TouchHandler/>
      <Dolly/>
      {children}
    </Canvas>
  )
}