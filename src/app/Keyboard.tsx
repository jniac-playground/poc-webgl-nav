import { useEffect } from 'react'
import { useStore } from '../state/main'
import { tweenTimelineHead } from '../state/timeline'

export const Keyboard = () => {
  const { sectionIndex } = useStore()
  useEffect(() => {
    const onKeyUp = (event:KeyboardEvent) => {
      if (event.key === 'ArrowDown') {
        tweenTimelineHead(sectionIndex + 1)
      }
      if (event.key === 'ArrowUp') {
        tweenTimelineHead(sectionIndex - 1)
      }
    }
    document.addEventListener('keyup', onKeyUp)
    return () => {
      document.removeEventListener('keyup', onKeyUp)
    }
  })
  return null
}
