import create from 'zustand'
import { combine, persist } from 'zustand/middleware'
import { SplitPage } from './SplitPage'

const storageName = 'poc-site-3d'
const fromStorage = <T>(key:string, defaultValue:T):T => {
  const state = JSON.parse(localStorage.getItem(storageName) ?? '{}')?.state ?? {}
  return key in state ? state[key] : defaultValue
}

export const splitPage = new SplitPage()
splitPage.sideOpen = fromStorage('sidePageExtended', false)

export const useStore = create(persist(combine({

  sidePageExtended: false,
  sectionIndex: 0,

}, set => ({
  
  setSidePageExtended: (value:boolean|'toggle') => set(() => {
    const sidePageExtended = splitPage.setSideOpen(value).sideOpen
    return { sidePageExtended }
  }),

  setSectionIndex: (value:number) => set(() => ({ sectionIndex:value })),

})), {

  name: storageName,
  deserialize: str => {
    // NOTE: here could be a hook for splitPage.sideOpen (and avoid double deserialization)
    return JSON.parse(str)
  },
  
}))

