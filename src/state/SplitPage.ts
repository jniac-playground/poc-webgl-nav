import { tween } from '../anim/Animator'

const getSideMax = () => Math.min(600, window.innerWidth)

export const SIDE_MIN = 64
export let SIDE_MAX = getSideMax()

export class SplitPage {
  #sideOpen = false
  #side = { opening:0 }
  #onUpdateSet = new Set<(opening:number) => void>()
  #onUpdate = () => {
    const { opening } = this.#side
    for (const cb of this.#onUpdateSet) {
      cb(opening)
    }
  }
  setSideOpen(value:boolean | 'toggle', duration = .5) {
    if (value === 'toggle') {
      value = !this.#sideOpen
    }
    if (this.#sideOpen === value) {
      return this
    }
    this.#sideOpen = value
    const to = value ? 1 : 0
    tween(this.#side, 'opening', { to, duration, onUpdate:this.#onUpdate })
    return this
  }
  subscribe(cb:(opening:number) => void) {
    this.#onUpdateSet.add(cb)
    const { opening } = this.#side
    cb(opening)
    return () => { this.#onUpdateSet.delete(cb) }
  }
  get sideOpen() { return this.#sideOpen }
  set sideOpen(value:boolean) { this.setSideOpen(value, 0) }
  get opening() { return this.#side.opening }
}
