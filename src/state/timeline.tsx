import { tween } from '../anim/Animator'
import * as flex from '../timeline/flex'
import { Timeline, TimelineNode } from '../timeline/Timeline'
import { LEVEL_COUNT } from './config'

export const SECTION_HEIGHT = 500

export const timeline = new Timeline({ svgShowRange:true })
timeline.head.position = SECTION_HEIGHT / 2

for (let i = 0; i < LEVEL_COUNT; i++) {
  const trigger = new TimelineNode({ size:'80%', sectionIndex:i, type:'section-trigger' })
  timeline.add(new TimelineNode({ size:SECTION_HEIGHT }).add(
    trigger
  ))
}

flex.compute(timeline)

export const tweenTimelineHead = (index:number, { 
  duration = 1, 
  ease = undefined,
}:Partial<{ 
  duration:number, 
  ease:string,
}> = {}) => {
  
  console.log(`tweenTimelineHead -> ${index} (${duration}s)`)
  const node:TimelineNode = timeline.find((n:TimelineNode) => n.style.sectionIndex === index)
  if (!node) {
    return
  }
  const to = node.bounds!.center
  tween(timeline.head, 'position', { to, duration, ease })
}


let wheelDelta = 0
window.addEventListener('wheel', event => {
  event.preventDefault()
  // wheelDelta = event.deltaY
  wheelDelta += (event.deltaY - wheelDelta) / 2
}, { passive:false })

let touchDelta = 0
export const setTouchDelta = (x:number) => touchDelta = x

const loop = () => {
  
  const min = SECTION_HEIGHT / 2
  const max = timeline.bounds!.size - SECTION_HEIGHT / 2
  
  timeline.head.position += wheelDelta
  wheelDelta += (0 - wheelDelta) / 2
  
  timeline.head.position += touchDelta
  touchDelta += (0 - touchDelta) * .1
  
  if (timeline.head.position < min) {
    timeline.head.position += (min - timeline.head.position) / 2
  }

  if (timeline.head.position > max) {
    timeline.head.position += (max - timeline.head.position) / 2
  }
  
  requestAnimationFrame(loop)
}
requestAnimationFrame(loop)

Object.assign(window, {
  timeline,
})
