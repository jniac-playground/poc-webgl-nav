# POC WebGL Nav

## Demo
[poc-webgl-nav via (gitlab pages)](https://jniac-playground.gitlab.io/poc-webgl-nav/)

<a href="https://jniac-playground.gitlab.io/poc-webgl-nav/">
  <img src="readme/screenshot-1.png" width="500">
</a>

## Stack
- [Create React App](https://github.com/facebook/create-react-app)
- [React Three Fiber](https://github.com/pmndrs/react-three-fiber)
- [three.js (of course)](https://threejs.org/docs/#manual/en/introduction/Creating-a-scene)
